===WP 404 to Server Log ===
Contributors: devPHASE.io
Donate link:
Tags: error, log, 404
Requires at least: 3.8
Tested up to: 5.3
Stable tag: 1.0

Logs 404 (Page Not Found) errors to your local server log file for your WordPress site.

== Description ==

Logs 404 (Page Not Found) errors to your local server log file for your WordPress site. This allows any log monitoring or scanning software to detect these errors.

== Frequently Asked Questions ==

== Changelog ==

= 1.0 =
* Initial Release

== Installation ==

1. Upload the plugin folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. The plugin settings can be accessed via the '404 Error Log' menu in the administration area
