<?php
/**
* Plugin Name: WP 404 to Server Log
* Plugin URI: https://devphase.io/
* Description: Plugin to log 404 (Page or File Not Found) errors to file on your site.
* Version: 1.0
* Author: devPHASE.io
* Author URI: https://devphase.io
* License: GPL2
* Text Domain: 404-error-log
*/

if( ! defined( 'ABSPATH' ) )
	exit;

if ( ! class_exists( 'Log_404' ) ) {
	class Log_404 {
		const opt = '404_log_options';
		private $options;

		function __construct() {
			// load options
			$this->options = get_option( self::opt, array() );

			if ( ! isset( $this->options['general']['log_enabled'] ) ) {
				$this->options['general']['log_enabled'] = 0;
			}
			if ( ! isset( $this->options['general']['file_path'] ) ) {
				$this->options['general']['file_path'] = '';
			}

			add_action( 'template_redirect', array( $this, 'log_404s' ) );

			if ( is_admin() ) {
				add_action( 'admin_init', array( $this, 'plugin_settings' ) );
				add_action( 'admin_menu', array( $this, 'plugin_menu' ) );
			}
		}

		public function plugin_menu() {
			add_menu_page(
				__( '404 Error Log', '404-error-log' ),
				__( '404 Error Log', '404-error-log' ),
				'manage_options',
				'404_error_log',
				array( $this, 'settings_page' ),
				'dashicons-groups',
				'88.025'
			);

			add_submenu_page(
				'404_error_log',
				__( '404 Error Log', '404-error-log' ),
				__( '404 Error Log', '404-error-log' ),
				'manage_options',
				'404_error_log',
				array( $this, 'settings_page' )
			);
		}

		public function plugin_settings() {
			register_setting(
				'404_log_options',
				'404_log_options',
				array( $this, 'sanitize_callback' )
			);

			add_settings_section(
				'404_log_options',
				__( 'General Settings', '404-error-log' ),
				'',
				'404_error_log'
			);

			add_settings_field(
				'log_enabled',
				__( 'Enable 404 Error Logging', '404-error-log' ),
				array( $this, 'log_enabled_field' ),
				'404_error_log',
				'404_log_options'
			);

			add_settings_field(
				'file_path',
				__( 'Path to Error Log File', '404-error-log' ),
				array( $this, 'file_path_field' ),
				'404_error_log',
				'404_log_options'
			);
		}

		public function sanitize_callback( $options ) {

			if ( empty( $options['general'] ) ) {
				return $options;
			}

			foreach ( $options['general'] as $name => &$val ) {

				if ( $name == 'log_enabled' ) {
					$val = intval( $val );
				}

				switch ( $name ) {
					case 'log_enabled':
						$val = intval( $val );
						break;

					case 'file_path':
						$val = sanitize_text_field( $val );
						break;

					default:
						break;
				}
			}

			return $options;
		}

		public function log_enabled_field() {
			$val = $this->options['general']['log_enabled']; ?>

			<label>
				<input type="checkbox" name="404_log_options[general][log_enabled]" id="log_enabled" value="1" <?php checked( 1, $val ); ?> />
				<span class="description"><?php _e( 'Enable/Disable', '404-error-log' ); ?></span>
			</label>

		<?php }

		public function file_path_field() {
			$val = $this->options['general']['file_path']; ?>

			<input type="text" name="404_log_options[general][file_path]" id="file_path" value="<?php echo esc_attr( $val ); ?>" placeholder="<?php _e( 'Path to file', '404-error-log' ); ?>"/>
			<br/>
			<span class="description"><?php _e( 'Enter a full path to file', '404-error-log' ); ?></span>

		<?php }

		/**
		 *  admin_menu_page callback function for render plugin Settings page
		 */
		public function settings_page() { ?>
			<div class="wrap">
				<h1><?php echo get_admin_page_title(); ?></h1>
				<form action="<?php echo admin_url( 'options.php' ); ?>" method="POST">
					<?php settings_fields( '404_log_options' );
					do_settings_sections( '404_error_log' );
					submit_button(); ?>
				</form>
			</div>
		<?php }

		/**
		 * @return bool
		 */
		function log_404s() {
			$logging_enabled = $this->options['general']['log_enabled'];
			$log_file = $this->options['general']['file_path'];

			if ( ! is_404() || empty( $logging_enabled ) || empty( $log_file ) ) {
				return false;
			}

			$log_file     = $this->options['general']['file_path'];
			$message_type = 3;

			$data = array(
				'path'    => '"' . $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI'] . '"' . ' failed (2: No such file or directory)',
				'client'  => 'client: ' . $_SERVER['REMOTE_ADDR'],
				'server'  => 'server: ' . $_SERVER['SERVER_NAME'],
				'request' => 'request: ' . '"' . $_SERVER['REQUEST_METHOD'] . ' ' . $_SERVER['REQUEST_URI'] . ' ' . $_SERVER['SERVER_PROTOCOL'] . '"',
				'host'    => 'host: ' . '"' . $_SERVER['HTTP_HOST'] . '"',
			);

			if ( ! empty( $_SERVER['HTTP_REFERER'] ) ) {
				$data['referrer'] = 'referrer: ' . '"' . $_SERVER['HTTP_REFERER'] . '"';
			}

			$message = current_time( 'Y/m/d H:i:s' ) . ' [error]: open() ' . implode( ', ', $data ) . "\n";

			$res = error_log( $message, $message_type, $log_file );

			return $res;
		}
	}
}

new Log_404();
